import React, { useState } from "react";
  
const App = () => {
  const [counter, setCounter] = useState(0)
  
  const handleClick1 = () => {
    setCounter(counter + 1)
  }
  
  const handleClick2 = () => {
    setCounter(counter - 1)
  }
  
  return (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: '300%',
      position: 'absolute',
      width: '100%',
      height: '100%',
      top: '-15%',
      backgroundColor: 'lightblue',

    }}>
      <div style={{
        fontSize: '120%',
        position: 'relative',
        top: '10vh',
      }}>
        {counter}
      </div>
      <div className="buttons">
        <button style={{
          fontSize: '80%',
          position: 'relative',
          top: '20vh',
          marginRight: '-25px',
          backgroundColor: 'black',
          borderRadius: '5%',
          color: 'lightblue',
          width: '65px' ,
        }}
          onClick={handleClick1}>+</button>
        <button style={{
          fontSize: '80%',
          position: 'relative',
          top: '20vh',
          marginLeft: '25px',
          backgroundColor: 'black',
          borderRadius: '5%',
          color: 'lightblue',
          width: '65px' ,

        }}
          onClick={handleClick2}>–</button>
      </div>
    </div>
  )
}
  
export default App